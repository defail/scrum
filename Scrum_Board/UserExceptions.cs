﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Scrum_Board
{

    public class EntityAlreadyExistsException : ApplicationException
    {
        // Сводка:
        //     Инициализирует новый экземпляр класса EntityAlreadyExistsException.
        public EntityAlreadyExistsException() { }
        //
        // Сводка:
        //     Выполняет инициализацию нового экземпляра класса EntityAlreadyExistsException, используя
        //     указанное сообщение об ошибке.
        //
        // Параметры:
        //   message:
        //     Сообщение, описывающее ошибку.
        public EntityAlreadyExistsException(string message) : base(message) { }
        //
        // Сводка:
        //     Инициализирует новый экземпляр класса EntityAlreadyExistsException указанным сообщением
        //     об ошибке и ссылкой на внутреннее исключение, которое стало причиной данного
        //     исключения.
        //
        // Параметры:
        //   message:
        //     Сообщение об ошибке с объяснением причин исключения.
        //
        //   innerException:
        //     Исключение, вызвавшее текущее исключение, или указатель null (Nothing в Visual
        //     Basic), если внутреннее исключение не задано.
        public EntityAlreadyExistsException(string message, Exception inner) : base(message, inner) { }

        protected EntityAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    public class EntityNotFoundException : ApplicationException
    {
        public EntityNotFoundException() { }

        public EntityNotFoundException(string message) : base(message) { }

        public EntityNotFoundException(string message, Exception inner) : base(message, inner) { }

        protected EntityNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
