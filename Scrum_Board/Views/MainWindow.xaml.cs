﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Scrum_Board.ViewModels;
namespace Scrum_Board
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public UserDataModel CurrentUser;
        public ProjectDataModel CurrentProject = new ProjectDataModel();
        public SprintDataModel CurrentSprint;
        
        AddProjectViewModel _AddProjectViewModel;
        ProjectsViewModel _ProjectsViewModel;
        AddSprintViewModel _AddSprintViewModel;
        public MainWindow(UserDataModel user)
        {
            InitializeComponent();
            CurrentUser = user;
            _AddProjectViewModel = new ViewModels.AddProjectViewModel(CurrentUser);
            _ProjectsViewModel = new ProjectsViewModel(CurrentUser);
            

            ProjectSettings.DataContext = _ProjectsViewModel;
            ProjectManager.DataContext = _AddProjectViewModel;

            _AddSprintViewModel = new AddSprintViewModel();
            SprintManagment.DataContext = _AddSprintViewModel;
        }
                
       
        private void LogOutClick(object sender, RoutedEventArgs e)
        {
            File.Delete(@"..\..\remember.bin");
            CurrentUser = null;
            UserLabel.Content = "";
            var AuthWindow = new Views.AuthenticationView();
            AuthWindow.Show();
            this.Close();
        }


    }

}
