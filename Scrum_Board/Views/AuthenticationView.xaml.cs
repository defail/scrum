﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Scrum_Board.Views
{
    /// <summary>
    /// Логика взаимодействия для Autorization.xaml
    /// </summary>
    public partial class AuthenticationView
    {
        ViewModels.AuthenticationViewModel ViewModel = new ViewModels.AuthenticationViewModel();
        public AuthenticationView()
        {
            InitializeComponent();
            this.DataContext = ViewModel;
            if (ViewModel.IfRememberMethod()) this.Close();
        }
        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            { ((dynamic)this.DataContext).LogInPassword = ((PasswordBox)sender).Password; }
        }

        private void LogInResult_Checked(object sender, RoutedEventArgs e)
        {
            var mainW = new MainWindow(ViewModel.User);
            mainW.CurrentUser = ViewModel.User;
            mainW.Show();
            this.Close();
        }

        private void PasswordFirst_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            { ((dynamic)this.DataContext).User.Password = ((PasswordBox)sender).Password; }
        }
    }
}
