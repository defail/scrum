﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Scrum_Board.EDM;

namespace Scrum_Board
{
    class SubtaskRepository:IRepository<SubtaskDataModel>
    {
        ScrumboardEntities context;
        public SubtaskRepository()
        {
        }
        public IEnumerable<SubtaskDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Subtasks.Select(s => new SubtaskDataModel()
                {
                    IdSubtask = s.Id_Subtask,
                    Task_Id=s.Task_Id,
                    Name = s.Name,
                    Comment = s.Comment,
                    SetTime = (TimeSpan)s.Set_Time,
                    ElapsedTime = (TimeSpan)s.Elapsed_Time,
                    Status = s.Status
                }).ToList<SubtaskDataModel>();
            }
        }

        public SubtaskDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.Subtasks.First(o => o.Id_Subtask == id);
                return new SubtaskDataModel()
                {
                    IdSubtask = s.Id_Subtask,
                    Task_Id=s.Task_Id,
                    Name = s.Name,
                    Comment = s.Comment,
                    SetTime = (TimeSpan)s.Set_Time,
                    ElapsedTime = (TimeSpan)s.Elapsed_Time,
                    Status = s.Status
                };
            }
        }

        public void Add(SubtaskDataModel entity)
        {
            //using  Context object
            using (var context = new ScrumboardEntities())
            {
                //id check for uniqueness
                if (!context.Subtasks.Any(s => s.Id_Subtask == entity.IdSubtask))
                {
                    context.Subtasks.Add(new Subtask()
                    {
                        Name = entity.Name,
                        Task_Id=entity.Task_Id,
                        Comment = entity.Comment,
                        Set_Time = entity.SetTime,
                        Elapsed_Time = entity.ElapsedTime,
                        Status = entity.Status
                    });
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException();
            }
        }

        public void Update(SubtaskDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Subtasks.Find(entity.Task_Id);
                if (temp != null)
                {
                    temp.Name = entity.Name;
                    temp.Comment = entity.Comment;
                    temp.Set_Time = entity.SetTime;
                    temp.Elapsed_Time = entity.ElapsedTime;
                    temp.Status = entity.Status;
                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(SubtaskDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Subtasks.Find(entity.IdSubtask);
                if (temp != null)
                {
                    context.Subtasks.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }
    }
}
