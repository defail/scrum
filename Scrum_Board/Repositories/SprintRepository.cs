﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Scrum_Board.EDM;

namespace Scrum_Board
{
    class SprintRepository:IRepository<SprintDataModel>
    {
        ScrumboardEntities context;
        public SprintRepository ()
        {
        }
        public IEnumerable<SprintDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Sprints.Select(s => new SprintDataModel()
                {
                    Id = s.Id,
                    Name=s.Name,
                    DateCreate=(DateTime)s.DateCreate,
                    DateClose = (DateTime)s.DateClose,
                    ProjectId=s.Project_Id
                }).ToList<SprintDataModel>();
            }
        }

        public SprintDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.Sprints.First(o => o.Id == id);
                return new SprintDataModel()
                {
                    Id = s.Id,
                    Name = s.Name,
                    DateCreate = (DateTime)s.DateCreate,
                    DateClose = (DateTime)s.DateClose,
                    ProjectId = s.Project_Id
                };
            }
        }

        public void Add(SprintDataModel entity)
        {
            //using  Context object
            using (var context = new ScrumboardEntities())
            {
                //login check for uniqueness
                if (!context.Sprints.Any(s => s.Id == entity.Id))
                {
                    context.Sprints.Add(new Sprint()
                    {
                        Name = entity.Name,
                        DateCreate = (DateTime)entity.DateCreate,
                        DateClose = (DateTime)entity.DateClose,
                        Project_Id = entity.ProjectId
                    });
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException();
            }
        }

        public void Update(SprintDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Sprints.Find(entity.Id);
                if (temp != null)
                {
                    temp.Name = entity.Name;
                    temp.DateCreate = entity.DateCreate;
                    temp.DateClose = entity.DateClose;
                    temp.Project_Id = entity.ProjectId;
                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(SprintDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Sprints.Find(entity.Id);
                if (temp != null)
                {
                    context.Sprints.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public IEnumerable<SprintDataModel> GetByProjectId(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Sprints.Where(p => p.Project_Id == id).Select(s => new SprintDataModel()
                {
                    Id = s.Id,
                    Name = s.Name,
                    DateCreate = (DateTime)s.DateCreate,
                    DateClose = (DateTime)s.DateClose,
                    ProjectId = s.Project_Id
                }).ToList<SprintDataModel>();
            }
        }
    }
}
