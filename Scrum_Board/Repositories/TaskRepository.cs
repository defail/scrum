﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Scrum_Board.EDM;

namespace Scrum_Board
{
    class TaskRepository : IRepository<TaskDataModel>
    {
        ScrumboardEntities context;
        public TaskRepository()
        {
        }
        public IEnumerable<TaskDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Tasks.Select(s => new TaskDataModel()
                {
                    Id = s.Id,
                    Name = s.Name,
                    Comment = s.Comment,
                    SetTime = (TimeSpan)s.Set_Time,
                    ProjectId = s.Project_Id,
                    SprintId = (int)s.Sprint_Id,
                    UserId = (int)s.User_Id,
                    ElapsedTime = (TimeSpan)s.Elapsed_Time,
                    Status = s.Status
                }).ToList<TaskDataModel>();
            }
        }

        public TaskDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.Tasks.First(o => o.Id == id);
                return new TaskDataModel()
                {
                    Id = s.Id,
                    Name = s.Name,
                    Comment = s.Comment,
                    SetTime = (TimeSpan)s.Set_Time,
                    ProjectId = s.Project_Id,
                    SprintId = (int)s.Sprint_Id,
                    UserId = (int)s.User_Id,
                    ElapsedTime = (TimeSpan)s.Elapsed_Time,
                    Status = s.Status
                };
            }
        }

        public void Add(TaskDataModel entity)
        {
            //using  Context object
            using (var context = new ScrumboardEntities())
            {
                //id check for uniqueness
                if (!context.Tasks.Any(s => s.Id == entity.Id))
                {
                    context.Tasks.Add(new Task()
                    {
                        Name = entity.Name,
                        Comment = entity.Comment,
                        Set_Time = entity.SetTime,
                        Project_Id = entity.ProjectId,
                        Sprint_Id = entity.SprintId,
                        User_Id = entity.UserId,
                        Elapsed_Time = entity.ElapsedTime,
                        Status = entity.Status
                    });
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException();
            }
        }

        public void Update(TaskDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Tasks.Find(entity.Id);
                if (temp != null)
                {
                    temp.Name = entity.Name;
                    temp.Comment = entity.Comment;
                    temp.Set_Time = entity.SetTime;
                    temp.Project_Id = entity.ProjectId;
                    temp.Sprint_Id = entity.SprintId;
                    temp.User_Id = entity.UserId;
                    temp.Elapsed_Time = entity.ElapsedTime;
                    temp.Status = entity.Status;
                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(TaskDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Tasks.Find(entity.Id);
                if (temp != null)
                {
                    context.Tasks.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }
        public IEnumerable<TaskDataModel> GetByProjectId(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Tasks.Where(p => p.Project_Id == id).Select(s => new TaskDataModel()
                {
                    Id = s.Id,
                    Name = s.Name,
                    Comment = s.Comment,
                    SetTime = (TimeSpan)s.Set_Time,
                    ProjectId = s.Project_Id,
                    SprintId = (int)s.Sprint_Id,
                    UserId = (int)s.User_Id,
                    ElapsedTime = (TimeSpan)s.Elapsed_Time,
                    Status = s.Status
                }).ToList<TaskDataModel>();;
            }
        }
        public IEnumerable<TaskDataModel> GetBySprintId(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Tasks.Where(p => p.Sprint_Id == id).Select(s => new TaskDataModel()
                {
                    Id = s.Id,
                    Name = s.Name,
                    Comment = s.Comment,
                    SetTime = (TimeSpan)s.Set_Time,
                    ProjectId = s.Project_Id,
                    SprintId = (int)s.Sprint_Id,
                    UserId = (int)s.User_Id,
                    ElapsedTime = (TimeSpan)s.Elapsed_Time,
                    Status = s.Status
                }).ToList<TaskDataModel>(); ;
            }
        }
    }
}
