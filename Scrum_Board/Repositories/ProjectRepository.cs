﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Scrum_Board.EDM;

namespace Scrum_Board
{
    class ProjectRepository : IRepository<ProjectDataModel>
    {
        ScrumboardEntities context;

        public ProjectRepository()
        {
        }
        public IEnumerable<ProjectDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Projects.Select(s => new ProjectDataModel()
                {
                    Id = s.Id,
                    Name = s.Name,
                    DateStart = (DateTime)s.DateStart,
                    DateEnd = (DateTime)s.DateEnd,
                    Status = s.Status,
                    TestingField = (bool)s.Testing_Field
                }).ToList<ProjectDataModel>();

            }
        }

        public ProjectDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.Projects.FirstOrDefault(o => o.Id == id);
                return new ProjectDataModel()
                {
                    Id = s.Id,
                    Name = s.Name,
                    DateStart = (DateTime)s.DateStart,
                    DateEnd = (DateTime)s.DateEnd,
                    Status = s.Status,
                    TestingField = (bool)s.Testing_Field
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Project id</returns>
        public void Add(ProjectDataModel entity)
        {
            //using  Context object

            using (var context = new ScrumboardEntities())
            {
                //login check for uniqueness
               
                if (!context.Projects.Any(s => s.Id == entity.Id))
                {
                    var project = new Project()
                    {
                        Name = entity.Name,
                        DateStart = (DateTime)entity.DateStart,
                        DateEnd = (DateTime)entity.DateEnd,
                        Status = entity.Status,
                        Testing_Field = (bool)entity.TestingField
                    };
                    context.Projects.Add(project);                   
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException();
            }
        }

        public void Update(ProjectDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Projects.Find(entity.Id);
                if (temp != null)
                {
                        temp.Name = entity.Name;
                        temp.DateStart = (DateTime)entity.DateStart;
                        temp.DateEnd = (DateTime)entity.DateEnd;
                        temp.Status = entity.Status;
                        temp.Testing_Field = (bool)entity.TestingField;
                        
                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(ProjectDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Projects.Find(entity.Id);
                if (temp != null)
                {
                    context.Projects.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }
        public IEnumerable<ProjectDataModel> GetProjectsByUserId(int _id)
        {
            using (var context = new ScrumboardEntities())
            {
                //обьявление обьекта - списка елементов таблицы User_to_Project по указанному _id некоторого User-а
                var ProjectsOfUser = from item in context.User_to_Project
                        where item.User_Id==_id
                        select item;
                //походу неправильное linq выражение но что именно не так прописал хз...    
                var ProjectsList=new List<ProjectDataModel>();
                foreach (var item in ProjectsOfUser)
                {
                    var project=context.Projects.Find(item.Progject_Id);
                    ProjectsList.Add(new ProjectDataModel()
                    {
                        Id = project.Id,
                        Name = project.Name,
                        DateStart = (DateTime)project.DateStart,
                        DateEnd = (DateTime)project.DateEnd,
                        Status = project.Status,
                        TestingField = (bool)project.Testing_Field
                    });
                }
                return ProjectsList;
                }
        }
        public void Add(ProjectDataModel projectEntity, UserToProjetDataModel usToEntity) 
        {
            //using  Context object

            using (var context = new ScrumboardEntities())
            {
                //login check for uniqueness

                if (!context.Projects.Any(s => s.Id == projectEntity.Id))
                {
                    var project = new Project()
                    {
                        Name = projectEntity.Name,
                        DateStart = (DateTime)projectEntity.DateStart,
                        DateEnd = (DateTime)projectEntity.DateEnd,
                        Status = projectEntity.Status,
                        Testing_Field = (bool)projectEntity.TestingField
                    };

                    var userTo = new User_to_Project()
                    {
                        Progject_Id = project.Id,
                        User_Id = usToEntity.UserId,
                        Role = usToEntity.Role
                    };

                    context.Projects.Add(project);
                    context.User_to_Project.Add(userTo);
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException();
            }
        }
    }
}
