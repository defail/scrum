﻿using Scrum_Board.EDM;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrum_Board
{
    class UserToProjectRepository:IRepository<UserToProjetDataModel>
    {
        ScrumboardEntities context;
        public UserToProjectRepository()
        {  }
        public IEnumerable<UserToProjetDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                return context.User_to_Project.Select(s => new UserToProjetDataModel()
                {
                    Numbers=s.Numbers,
                    ProjectId=s.Progject_Id,
                    Role=s.Role,
                    UserId=s.User_Id
                }).ToList<UserToProjetDataModel>();
            }
        }

        public UserToProjetDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.User_to_Project.First(o => o.Numbers == id);
                return new UserToProjetDataModel()
                {
                    Numbers = s.Numbers,
                    ProjectId = s.Progject_Id,
                    Role = s.Role,
                    UserId = s.User_Id
                };
            }
        }

        public void Add(UserToProjetDataModel entity)
        {
            //using  Context object
            using (var context = new ScrumboardEntities())
            {
                //login check for uniqueness
              //  if (!context.User_to_Project.Any(s => s.Numbers == entity.Numbers))
              //  {
                    context.User_to_Project.Add(new User_to_Project()
                    {
                        Numbers = entity.Numbers,
                        Progject_Id = entity.ProjectId,
                        Role = entity.Role,
                        User_Id = entity.UserId
                    });
                    context.SaveChanges();
              //  }
              //  else throw new EntityAlreadyExistsException();
            }
        }

        public void Update(UserToProjetDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.User_to_Project.Find(entity.Numbers);
                if (temp != null)
                {
                    temp.Progject_Id = entity.ProjectId;
                    temp.User_Id = entity.UserId;
                    temp.Role = entity.Role;
                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(UserToProjetDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.User_to_Project.Find(entity.Numbers);
                if (temp != null)
                {
                    context.User_to_Project.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(int _selectedProject, int _selectedUser)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.User_to_Project.Where(p => p.User_Id == _selectedUser).Where(s => s.Progject_Id == _selectedProject).First();              
                if (temp != null)
                {
                    context.User_to_Project.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }
    }
}
