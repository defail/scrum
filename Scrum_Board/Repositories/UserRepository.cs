﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Scrum_Board.EDM;

namespace Scrum_Board
{
    class UserRepository : IRepository<UserDataModel>
    {
        public UserRepository()
        {
        }

        public IEnumerable<UserDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Users.Select(s => new UserDataModel()
                {
                    Id = s.Id,
                    Login = s.Login,
                    Password = s.Password,
                    FirstName = s.F_Name,
                    LastName = s.L_Name,
                    Photo = s.Photo,
                    Email = s.Email
                }).ToList<UserDataModel>();
            }

        }

        public UserDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.Users.First(o => o.Id == id);
                return new UserDataModel()
                {
                    Id = s.Id,
                    Login = s.Login,
                    Password = s.Password,
                    FirstName = s.F_Name,
                    LastName = s.L_Name,
                    Photo = s.Photo,
                    Email = s.Email
                };
            }
        }

        public void Add(UserDataModel entity)
        {
            //using  Context object
            using (var context = new ScrumboardEntities())
            {
                //login check for uniqueness
                if (!context.Users.Any(s => s.Login == entity.Login))
                {
                    context.Users.Add(new User()
                    {
                        Login = entity.Login,
                        Password = entity.Password,
                        F_Name = entity.FirstName,
                        L_Name = entity.LastName,
                        Photo = entity.Photo,
                        Email = entity.Email,
                    });
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException("This nickname are used");
            }
        }

        public void Update(UserDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Users.Find(entity.Id);
                if (temp != null)
                {
                    temp.Login = entity.Login;
                    temp.Password = entity.Password;
                    temp.F_Name = entity.FirstName;
                    temp.L_Name = entity.LastName;
                    temp.Photo = entity.Photo;
                    temp.Email = entity.Email;
                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(UserDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                try
                {
                    var temp = context.Users.Find(entity.Id);
                    context.Users.Remove(temp);
                    context.SaveChanges();
                }
                catch { throw new EntityNotFoundException("User not found."); }
            }
        }
        public UserDataModel GetByNickname(string _nickname)
        {
            using (var context = new ScrumboardEntities())
            {
                try
                {
                    var s = context.Users.First(o => o.Login == _nickname);
                    if(s!=null)
                    return new UserDataModel()
                        {
                            Id = s.Id,
                            Login = s.Login,
                            Password = s.Password,
                            FirstName = s.F_Name,
                            LastName = s.L_Name,
                            Photo = s.Photo,
                            Email = s.Email
                        };
                    else throw new EntityNotFoundException("Nickname not found.");
                }
                catch { throw new EntityNotFoundException("Nickname not found."); }
            }
        }
        public IEnumerable<UserDataModel> GetByProjectID(int _id)
        {
            using (var context = new ScrumboardEntities())
            {
                //обьявление обьекта - списка елементов таблицы User_to_Project по указанному _id некоторого User-а
                var UsersOfProject = from item in context.User_to_Project
                                     where item.Progject_Id == _id
                                     select item;
                var UsersList = new List<UserDataModel>();
                foreach (var item in UsersOfProject)
                {
                    var user = context.Users.Find(item.User_Id);
                    UsersList.Add(new UserDataModel()
                    {
                        Id = user.Id,
                        FirstName = user.F_Name,
                        LastName = user.L_Name,
                        Email = user.Email,
                        Login = user.Login,
                        Password = user.Password,
                        Photo = user.Photo
                    });
                }
                return UsersList;
            }
        }
    }
}
