﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrum_Board
{
    class SubtaskDataModel
    {
        public int IdSubtask { get; set; }
        public int Task_Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public TimeSpan SetTime { get; set; }
        public TimeSpan ElapsedTime { get; set; }
        public string Status { get; set; }
    }
}
