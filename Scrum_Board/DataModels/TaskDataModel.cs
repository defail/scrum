﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrum_Board
{
    class TaskDataModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public TimeSpan SetTime { get; set; }
        public TimeSpan ElapsedTime { get; set; }      
        public int ProjectId { get; set; }
        public int SprintId { get; set; }
        public int UserId { get; set; }
        public string Status { get; set; }
    }
}
