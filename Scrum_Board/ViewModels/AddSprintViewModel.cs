﻿using Framework.UI.Controls;
using Scrum_Board.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Scrum_Board.ViewModels
{
    class AddSprintViewModel: INotifyPropertyChanged
    {
        #region Constructor
        public AddSprintViewModel()
        {
            AddSprintCommand = new Command(arg => AddSprintMethod());
            StartSprintCommand = new Command(arg => StartSprintMethod());
            EndSprintCommand = new Command(arg => EndSprintMethod());
            ViewSprintsCommand = new Command(arg => ViewSprintsMethod());
            OpenSprintCommand = new Command(arg => OpenSprint());
            Sprint = new SprintDataModel();
        }
        #endregion Constructor

        #region Command
        public ICommand AddSprintCommand { get; set; }
        public ICommand StartSprintCommand { get; set; }
        public ICommand EndSprintCommand { get; set; }
        public ICommand ViewSprintsCommand { get; set; }
        public ICommand OpenSprintCommand { get; set; }


        #endregion Command

        #region Properties
        public SprintDataModel Sprint { get; set; }
        private ProjectDataModel CurrentProject;

        public ObservableCollection<SprintDataModel> SprintsCollection { get; set; }

        private SprintDataModel _selectedSprint;
        public SprintDataModel SelectedSprint
        {
            get { return _selectedSprint; }
            set
            {
                _selectedSprint = value;
                RaisePropertyChanged("SelectedSprint");
            }
        }

        #endregion Properties

        #region Methods
        private void ViewSprintsMethod()
        {
            CurrentProject = Application.Current.Windows.OfType<MainWindow>().Single().CurrentProject;

            SprintsCollection = new ObservableCollection<SprintDataModel>();
            try
            {
                if (CurrentProject != null)
                {
                    var rep = new SprintRepository();
                    foreach (var item in rep.GetByProjectId(CurrentProject.Id))
                    {
                        SprintsCollection.Add(item);
                    }
                }

            }
            catch (EntityAlreadyExistsException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }
        private void AddSprintMethod()
        {
            try
            {
                var rep = new SprintRepository();
                Sprint.ProjectId = Application.Current.Windows.OfType<MainWindow>().Single().CurrentProject.Id;
                rep.Add(Sprint);
                MessageDialog.ShowAsync("Sprint " + Sprint.Name.Trim() + " are created.", null);         
            }
            catch (EntityNotFoundException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }

        private void StartSprintMethod()
        {
            try
            {
                var rep = new SprintRepository();
                _selectedSprint.DateCreate = DateTime.Now;
                rep.Update(_selectedSprint);
                MessageDialog.ShowAsync("Sprint " + _selectedSprint.Name.Trim() + " are started.", null);
            }
            catch (EntityNotFoundException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }

        private void EndSprintMethod()
        {
            try
            {
                var rep = new SprintRepository();
                _selectedSprint.DateClose = DateTime.Now;
                rep.Update(_selectedSprint);
                MessageDialog.ShowAsync("Sprint " + _selectedSprint.Name.Trim() + " are ended.", null);
            }
            catch (EntityNotFoundException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }

        private void OpenSprint()
        {
            var mainV = new SprintView();
            mainV.Show();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            Application.Current.Windows.OfType<MainWindow>().Single().CurrentSprint = _selectedSprint;
        }

        private void SprintListPropertychanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion Methods
    }
}
