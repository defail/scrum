﻿using Framework.UI.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Elysium.Controls;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

//ddas
namespace Scrum_Board.ViewModels
{
    class AuthenticationViewModel
    {
        #region Constructor
        //dfghjkl
        /// <summary>
        /// Constructor.
        /// </summary>
        public AuthenticationViewModel()
        {
            LogInCommand = new Command(arg => LogInMethod());
            SignUpCommand = new Command(arg => SignUpMethod());
            User = new UserDataModel();
            LogInPassword = "";
            LogInNickname = "";
            IfRemember = false;
            
        }

        #endregion

        #region Properties

        /// <summary>
        /// Получение или установление значения обьекта UserDataModel.
        /// </summary>
        public UserDataModel User { get; set; }
        /// <summary>
        /// Получение или установление значения строки Password.
        /// </summary>
        public string LogInPassword { get; set; }
        /// <summary>
        /// Получение или установление значения строки Nickname.
        /// </summary>
        public string LogInNickname { get; set; }
        public bool IfRemember { get; set; }


        #endregion

        #region Commands

        /// <summary>
        /// Get or set LogInCommand.
        /// </summary>
        public ICommand LogInCommand { get; set; }
        public ICommand SignUpCommand { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Click Login method.
        /// </summary>
        private void LogInMethod()
        {
            UserRepository _UserRepo = new UserRepository();
            try
            {
                User = _UserRepo.GetByNickname(LogInNickname);
                if (GetHashString(LogInPassword) == User.Password)
                {

                    if (IfRemember)
                    {
                        Remember();
                    }
                    else File.Delete(@"..\..\remember.bin");
                    OpenMain();
                }
                else
                {
                    User = null;
                    MessageDialog.ShowAsync("Password is wrong", null);
                }
            }
            catch (EntityNotFoundException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }
        /// <summary>
        /// Метод шифрования строки.
        /// </summary>
        private string GetHashString(string s)
        {
            //переводим строку в байт-массим  
            byte[] bytes = Encoding.Unicode.GetBytes(s);

            //создаем объект для получения средст шифрования  
            MD5CryptoServiceProvider CSP =
                new MD5CryptoServiceProvider();

            //вычисляем хеш-представление в байтах  
            byte[] byteHash = CSP.ComputeHash(bytes);

            string hash = string.Empty;

            //формируем одну цельную строку из массива  
            foreach (byte b in byteHash)
                hash += string.Format("{0:x2}", b);

            return hash;
        }
        /// <summary>
        /// Метод сохранния текущего пользователя в файл.
        /// </summary>
        private void Remember()
        {
            using (var fs = new FileStream(@"..\..\remember.bin", FileMode.Create))
            {
                var bf = new BinaryFormatter();
                bf.Serialize(fs, User);
            }
        }
        private void SignUpMethod()
        {
            try
            {
                UserRepository userRepo = new UserRepository();
                User.Password = GetHashString(User.Password);
                userRepo.Add(User);
                MessageDialog.ShowAsync("User " + User.Login + " are saved.", null);
            }
            catch (EntityAlreadyExistsException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }
        private void OpenMain()
        {
            var mainV = new MainWindow(User);
            mainV.DataContext = mainV.CurrentUser;
            mainV.Show();
            Application.Current.Windows.OfType<Views.AuthenticationView>().Single().Close();
        }

        public bool IfRememberMethod()
        {
            if (File.Exists(@"..\..\remember.bin"))
            {
                UserDataModel tempUser;
                using (var fs = new FileStream(@"..\..\remember.bin", FileMode.Open))
                {
                    var bf = new BinaryFormatter();
                    tempUser = (UserDataModel)bf.Deserialize(fs);
                }

                UserRepository _UserRepo = new UserRepository();
                try
                {
                    User = _UserRepo.GetByNickname(tempUser._Login);
                    if (tempUser._Password == User.Password)
                    {
                        var mainV = new MainWindow(User);
                        mainV.DataContext = mainV.CurrentUser;
                        mainV.Show();
                    }
                    else
                    {
                        User = null;
                    }
                }
                catch (EntityNotFoundException ex)
                {
                    MessageDialog.ShowAsync(ex.Message, null);
                }
                return true;
            }
            else return false;

        }


        #endregion
    }
}
