﻿using Framework.UI.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Scrum_Board.ViewModels
{
    class SprintManagmentViewModel: INotifyPropertyChanged
    {
        #region Constructor
        public SprintManagmentViewModel(SprintDataModel _sprint, ProjectDataModel _project)
        {
            ViewAllTasksCommand = new Command(arg => ViewAllTasksMethod());
            AddTaskCommand = new Command(arg => AddTaskMethod());
            RemoveTaskCommand = new Command(arg => RemoveTaskMethod());
            ViewSprintTasksCommand = new Command(arg => ViewSprintTasksMethod());
            CurrentSprint = _sprint;
            CurrentProject = _project;
            ViewAllTasksMethod();
            ViewSprintTasksMethod();
        }
        #endregion Constructor

        #region Command
        public ICommand ViewAllTasksCommand { get; set; }
        public ICommand AddTaskCommand { get; set; }
        public ICommand RemoveTaskCommand { get; set; }
        public ICommand ViewSprintTasksCommand { get; set; }
        #endregion Command

        #region Properties
        private SprintDataModel CurrentSprint;
        private ProjectDataModel CurrentProject;
        public ObservableCollection<TaskDataModel> ProjectTasksCollection { get; set; }

        public ObservableCollection<TaskDataModel> SprintTasksCollection { get; set; }

        private TaskDataModel _selectedTask;
        public TaskDataModel SelectedTask
        {
            get { return _selectedTask; }
            set
            {
                _selectedTask = value;
                RaisePropertyChanged("SelectedTask");
            }
        }

        #endregion Properties

        #region Methods
        private void ViewAllTasksMethod()
        {

            ProjectTasksCollection = new ObservableCollection<TaskDataModel>();
            try
            {

                var rep = new TaskRepository();
                foreach (var item in rep.GetByProjectId(CurrentProject.Id))
                {
                    ProjectTasksCollection.Add(item);
                }

            }
            catch (EntityAlreadyExistsException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }
        private void AddTaskMethod()
        {
            try
            {
                TaskRepository rep = new TaskRepository();
                _selectedTask.SprintId = CurrentSprint.Id;
                rep.Update(_selectedTask);
                ViewSprintTasksMethod();
            }
            catch (EntityNotFoundException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }
        private void RemoveTaskMethod()
        {
            try
            {
                TaskRepository rep = new TaskRepository();
                _selectedTask.SprintId = 0;
                rep.Update(_selectedTask);
                MessageDialog.ShowAsync("Task " + _selectedTask.Name.Trim() + " are removed.", null);
                ViewSprintTasksMethod();
            }
            catch (EntityAlreadyExistsException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }
        private void ViewSprintTasksMethod()
        {
            SprintTasksCollection = new ObservableCollection<TaskDataModel>();
            try
            {

                var rep = new TaskRepository();
                foreach (var item in rep.GetBySprintId(CurrentSprint.Id))
                {
                    SprintTasksCollection.Add(item);
                }

            }
            catch (EntityAlreadyExistsException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UserListPropertychanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion Methods
    }
}
