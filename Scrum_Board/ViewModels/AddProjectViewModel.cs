﻿using Framework.UI.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Scrum_Board.ViewModels
{
    class AddProjectViewModel
    {
        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public AddProjectViewModel(UserDataModel _user)
        {
            AddProjectCommand = new Command(arg => AddProjectMethod()); 
            Project = new ProjectDataModel();
            UserToProject = new UserToProjetDataModel
            {
                UserId = _user._Id,
                Role = "Master"
            };
        }

        #endregion

        #region Properties

        /// <summary>
        /// Получение или установление значения обьекта ProjectDataModel.
        /// </summary>
        public ProjectDataModel Project { get; set; }
        /// <summary>
        /// Получение или установление значения строки ProjectName.
        /// </summary>      
        
        public UserToProjetDataModel UserToProject { get; set; }
        #endregion
        #region Commands
        /// <summary>
        /// Get or set LogInCommand.
        /// </summary>
        public ICommand AddProjectCommand { get; set; }

        #endregion

        #region Methods
        private void AddProjectMethod()
        {
            try
            {
                var rep = new ProjectRepository();
                Project.DateStart = DateTime.Now;
                Project.Status = "Active";
                rep.Add(Project, UserToProject);
                MessageDialog.ShowAsync("Project " + Project.Name + " are saved.", null);
            }
            catch (EntityAlreadyExistsException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }       
        #endregion

        
    }
}
        