﻿using Framework.UI.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Scrum_Board.ViewModels
{
    class ProjectsViewModel : INotifyPropertyChanged
    {
        public ProjectsViewModel(UserDataModel _user)
        {
            ViewProjectCommand = new Command(arg => GetAllProjectMethod());
            UserToProject = new UserToProjetDataModel
            {
                UserId = _user._Id
            };

            GetAllProjectMethod();
            AddUserCommand = new Command(arg => AddUserMethod());
            RemoveUserCommand = new Command(arg => RemoveUserMethod());
            ViewUsersCommand = new Command(arg => ViewUsersMethod());

            User = new UserDataModel();
            UserToProject = new UserToProjetDataModel();
        }
        public ICommand ViewProjectCommand { get; set; }
        public ICommand AddUserCommand { get; set; }
        public ICommand RemoveUserCommand { get; set; }
        public ICommand ViewUsersCommand { get; set; }


        public UserToProjetDataModel UserToProject { get; set; }

        private ProjectDataModel _selectedProject;
        
        public ObservableCollection<ProjectDataModel> ProjectsCollection { get; set; }

        public UserDataModel User { get; set; }

        
        private UserDataModel _selectedUser;
        private ObservableCollection<UserDataModel> _UsersCollection;
        public ObservableCollection<UserDataModel> UsersCollection
        {
            get { return _UsersCollection; }
            set
            {
                _UsersCollection = value;
                UserListPropertychanged("UsersCollection");
            }
        }

        public ProjectDataModel SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                RaisePropertyChanged("SelectedProject");
            }
        }
        public UserDataModel SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                _selectedUser = value;
                UserListPropertychanged("SelectedUser");
            }
        }
        private void GetAllProjectMethod()
        {

            ProjectsCollection = new ObservableCollection<ProjectDataModel>();
            try
            {

                var rep = new ProjectRepository();
                foreach (var item in rep.GetProjectsByUserId(UserToProject.UserId))
                {
                    ProjectsCollection.Add(item);
                }

            }
            catch (EntityAlreadyExistsException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }
        private void AddUserMethod()
        {
            try
            {
                UserToProjectRepository rep = new UserToProjectRepository();
                UserRepository usRep = new UserRepository();
                UserDataModel ThisUser = usRep.GetByNickname(User.Login);
                UserToProjetDataModel userTo = new UserToProjetDataModel
                {
                    UserId = ThisUser._Id,
                    ProjectId = _selectedProject.Id,
                    Role = "Developer"
                };
                rep.Add(userTo);
                MessageDialog.ShowAsync("User " + ThisUser.Login.Trim() + " are added.", null);
                ViewUsersMethod();
            }
            catch (EntityNotFoundException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }
        private void RemoveUserMethod()
        {
            try
            {
                UserToProjectRepository rep = new UserToProjectRepository();
                rep.Delete(_selectedProject.Id, _selectedUser._Id);
                MessageDialog.ShowAsync("User " + _selectedUser._Login.Trim() + " are removed.", null);
                ViewUsersMethod();
            }
            catch (EntityAlreadyExistsException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }
        }
        private void ViewUsersMethod()
        {
            UsersCollection = new ObservableCollection<UserDataModel>();
            if (SelectedProject != null) { 
            try
            {
                var rep = new UserRepository();
                foreach (var item in rep.GetByProjectID(SelectedProject.Id))
                {
                    UsersCollection.Add(item);
                }
            }
            catch (EntityAlreadyExistsException ex)
            {
                MessageDialog.ShowAsync(ex.Message, null);
            }}
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            Application.Current.Windows.OfType<MainWindow>().Single().CurrentProject = _selectedProject;
            ViewUsersMethod();
        }

        private void UserListPropertychanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}
